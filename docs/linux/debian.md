# Debian Server

This is a short overview to install a new debian  server. 

## New Host

First create the virtual (by cloning) or physical maschine with OS installation.

```bash
# configure hostname and ip
sudo hostnamectl set-hostname <NAME>
sudo vi /etc/hosts # change hostname in line 2
sudo vi /etc/network/interfaces # set IP address, netmask and gateway
sudo shutdown -r now
```

## Software Installation

### Automatic Patching

If you wish you can enable automatic patching with or without automatic reboot as you wish. If a problem occurs or a reboot is still necessary after the patching an email will be sent.

```bash
sudo apt install -y unattended-upgrades debian-goodies
sudo vi crontab # add the following lines
```

    PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    MAILTO="operations@company.org"
    0 1 * * 1 unattended-upgrades
    0 2 * * 1 test -f /var/run/reboot-required && checkrestart | mail -s "Reboot required for $(hostname -f)" $MAILTO

```bash
vi /etc/apt/apt.conf.d/50unattended-upgrades
# enable line with: Unattended-Upgrade::Automatic-Reboot "true";
```

!!! attention

    If you use it with a cluster do it one node after the other with enough time between. Also it is advisable to never patch more than one host at a time so that on problems only one host may be the cause at this specific time.

### Prometheus Monitoring

```bash
# enable the metrics on the host
sudo apt install -y prometheus-node-exporter
```

Then on the prometheus server you have to add it in the scraper config:

```yaml title="/etc/prometheus/prometheus.yml"
- job_name: "node"
  static_configs:
      - targets:
            - <DOMAIN_OR_IP>:9100
  relabel_configs:
      - source_labels: [__address__]
        regex: '([^:]+):\d+'
        target_label: instance
```

### etc Keeper

This will automatically store all files and changes within `/etc` in a git repository which will be pushed to your central git instance.

In the example below a git project will be created at `https://gitlab.local/etckeeper/<HOSTNAME>`. So you should have a group `etckeeper` with a user allowable to write there.

```bash
# install etckeeper
sudo apt install -y etckeeper
# setup remote
sudo sed -i 's/PUSH_REMOTE=""/PUSH_REMOTE="origin"/' /etc/etckeeper/etckeeper.conf
sudo ssh-keygen # if not already done
sudo cat /root/.ssh/id_rsa.pub
```

Now you have to add the ssh key in the git repository's user.

```bash
cd /etc
sudo git remote remove origin
sudo git remote add origin git@gitlab.local:etckeeper/$(hostname -f).git
sudo git push -u origin --all
```

If a project was not existing before it would be created for you. The update will be directly after apt changes or at least once a day.

### Docker

```bash
# add repos
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
wget -qO - https://azlux.fr/repo.gpg.key | sudo apt-key add -
echo "deb http://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
# remove previous installations
sudo apt remove docker docker-engine docker.io containerd runc
# install
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose docker-ctop
sudo systemctl start docker
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
# allow using docker without sudo
sudo usermod -a -G docker <USER>
```

## Distribution Upgrade

Instead of a complete new installation often an upgrade is also possible and seldom fails in the last years.

Pre Conditions:

-   green in Monitoring
-   a snapshot or backup is made

### Debian 9 -> 10

```bash
# update the current distribution
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
# switch repository
sed -i 's/stretch/buster/g' /etc/apt/sources.list
grep '^[^#]' /etc/apt/sources.list /etc/apt/sources.list.d/* | grep -v stretch && echo "Please have a look at the above repositories, should they be changed?"
# install new distribution
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove --purge -y
# reboot
sudo shutdown -r now
```

### Debian 10 -> 11

```bash
# update the current distribution
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
# switch repository
echo -e "deb http://deb.debian.org/debian bullseye main contrib non-free\ndeb-src http://deb.debian.org/debian bullseye main contrib non-free\n\ndeb http://deb.debian.org/debian bullseye-updates main contrib non-free\ndeb-src http://deb.debian.org/debian bullseye-updates main contrib non-free\n\ndeb http://deb.debian.org/debian-security/ bullseye-security main contrib non-free\ndeb-src http://deb.debian.org/debian-security/ bullseye-security main contrib non-free\n" | sudo tee /etc/apt/sources.list
grep '^[^#]' /etc/apt/sources.list /etc/apt/sources.list.d/* | grep -v bullseye && echo "Please have a look at the above repositories, should they be changed?"
# install new distribution
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get install popularity-contest -y
sudo apt-get autoremove --purge -y
# reboot
sudo shutdown -r now
```

Known Problems:

-   redis: `/lib/systemd/system/redis-server.service` add `ReadWritePaths=-/var/local/redis`
    after that `sudo systemctl daemon-reload && sudo systemctl restart redis-server`
-   elasticsearch: Plugins are built for a specific version of Elasticsearch, and therefore must be reinstalled each time Elasticsearch is updated:

    ```bash
    for plugin in $(sudo /usr/share/elasticsearch/bin/elasticsearch-plugin list); do
      sudo /usr/share/elasticsearch/bin/elasticsearch-plugin remove $plugin
      sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install $plugin
    done
    ```

-   cifs: protocol version 1.0 is no longer possible, use `vers=2.1` or `vers=3.0`

{!docs/assets/abbreviations.txt!}
