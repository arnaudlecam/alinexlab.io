# bind

BIND is an open source system DNS server or an authoritative name server, and provides features like load balancing, notify, dynamic update, split DNS, DNSSEC, IPv6, and more.

## Local Cache

To speed up DNS resolution you may add a local DNS.

```bash
sudo apt install bind9
```

Now setup the name resolution:

```text title="/etc/bind/named.options"
// your default dns servers (isp, firewall, router, opendns, etc.)
forwarders {
    // DNS for internal networks
    10.10.10.10;
    10.10.10.11;
    172.17.100.10;
    172.17.100.11;
    // google-public-dns-a.google.com
    8.8.8.8; 
};
// locally no dnssec needed
dnssec-enable no;
dnssec-validation no;
```

Now setup name resolution to use local nameserver:

```text title="resolv.conf"
/etc/resolvconf/resolv.d/tail
nameserver 127.0.0.1
```

## Nameserver per Domain

You can specify a specific nameserver for each domain you want to be resolved:

```text title="/etc/bind/named.conf.local"
zone "test.local" IN {
    type forward;
    forwarders {172.17.10.10; 172.17.10.11;};
};

zone "stage.local" IN {
    type forward;
    forwarders {172.17.10.10; 172.17.10.11;};
};

{!docs/assets/abbreviations.txt!}
