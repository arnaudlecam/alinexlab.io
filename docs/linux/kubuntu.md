title: Kubuntu

# Kubuntu Desktop System

This is a short overview what I installed and configured on my new laptop (2022). And with which I am working since then.

!!! info

    I use a HP EliteBook 640G but will not go in detail about hardware settings because Ubuntu will take care of all these.
    The following is based on my personal needs.

## OS Installation

To install I had to disable UEFI and switch the BIOS into legacy mode. After that booting with a bootable USB stick (used etcher to create it) I could start the installation. I installed Kubuntu 20.4.4 LTE.

### Sudo without password

Add a new file for user allowance:

```text title="sudo vi /etc/sudoers.d/50-user"
alex ALL=(ALL) NOPASSWD: ALL
```

## KDE Configuration

-  Global design -> select "Breeze Dark"
-  Regional Settings -> add german language
-  Input devices > Touchpad -> enable tap to click and three finger middle click

## Chrome

As chrome is not in the default repository best way is to use the google repository:

```bash
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt update
sudo apt install chrome
```

## Other Software

From package manager:

- terminator
- git
- vim

Special installation:

-   VSCode: https://alinex.gitlab.io/env/vscode.html#installation
-   KeepassXC: https://alinex.gitlab.io/env/keepass.html#installation-of-keepassxc

What else to do:

-   VPN
-   Teamviewer

{!docs/assets/abbreviations.txt!}
