title: KDE

# KDE Plasma

The K Desktop Environment has lots of good tools to help be productive.

## Krunner

Launch Krunner from anywhere using ++alt+f2++ and type your command. It will just search while you type. 

-   You can type part of a **command**, **last used file** or **bookmark** to open it: `spec` to start Spectacle.
-   **Kill a command** by typing kill before, to show the process (with PID) to be killed: `kill terminator` 
-   Make **math calculations** by prepending the formula with `=`: `=10+20`


{!docs/assets/abbreviations.txt!}
