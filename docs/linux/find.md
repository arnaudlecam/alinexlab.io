title: find

# find - file search utility

Find is a very powerful GNU utility to find and work with files and directories. It traverses the directory tree and can search for specific elements and also delete them or give them to another command.

This should explain the general use and give some examples. Not all possibilities are explained here, use the man page for more details.

!!! info

    Usage: `find [-L] [<path>...] [<expression>...]`

If no `path` is given the current directory `.` is used to start the search.

The option `-L` will follow symbolic links and analyze the linked node instead of the link itself.

## Expressions

The expression will define how to search and match files and what to do with them. Here you see that find is very powerful and has a lot of options.

### Global options

Global options affect the operation of tests and actions specified on any part of the command line. To prevent confusion, global options should specified at the start of the whole expression.

| Expression             | Global Option                                                                                           |
| ---------------------- | ------------------------------------------------------------------------------------------------------- |
| `-d` or `-depth`       | Process each directory's contents before the directory itself (the `-delete` action also implies this). |
| `-maxdepth <levels>`   | How deep to go, 0 means only the starting point itself while 1 means also one level below.              |
| `-mindepth <levels>`   | Only work on files starting with the given number of levels below the starting point.                   |
| `-xdev` or `-mount`    | Don't go into other file systems.                                                                       |
| `-ignore_readdir_race` | No error message will appear if a file was deleted while the find is running.                           |

### Positional options

Positional options affect only tests or actions which follow them.

| Expression         | Positional Option                                                                                                                                                                                                                                                                               |
| ------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-daystart`        | Measure times (for `-amin`, `-atime`, `-cmin`, `-ctime`, `-mmin` and `-mtime`) from the beginning of today rather than from 24 hours ago.                                                                                                                                                       |
| `-regextype type`  | Set the regular expression syntax understood by `-regex` and `-iregex` tests which occur later on the command line. One of: `findutils-default`, `ed`, `emacs`, `gnu-awk`, `grep`, `posix-awk`, `awk`, `posix-basic`, `posix-egrep`, `egrep`, `posix-extended`, `posix-minimal-basic` or `sed`. |
| `-warn`, `-nowarn` | Turn warning messages on or off.                                                                                                                                                                                                                                                                |

### Test

Tests return a `true` or `false` value, usually on the basis of some property of a file we are considering.

!!! Info

    Numeric values can have a number for exact match or a prefix like `+` to match greater values or `-` to match lower values.

| Expression                 | Test                                                                                                                                                                                                                                         |
| -------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-amin <num>`              | File was last accessed `num` minutes ago.                                                                                                                                                                                                    |
| `-atime <num>`             | File was last accessed `num*24` hours ago.                                                                                                                                                                                                   |
| `-cmin <num>`              | File's status was last changed `num` minutes ago.                                                                                                                                                                                            |
| `-ctime <num>`             | File's status was last changed `num*24` hours ago.                                                                                                                                                                                           |
| `-mmin <num>`              | File's data was last modified `num` minutes ago.                                                                                                                                                                                             |
| `-mtime <num>`             | File's data was last modified `num*24` hours ago.                                                                                                                                                                                            |
| `-anewer <reference>`      | Time of the last access of the current file is more recent than that of the last data modification of the reference file.                                                                                                                    |
| `-cnewer <reference>`      | Time of the last status change of the current file is more recent than that of the last data modification of the reference file.                                                                                                             |
| `-newer <reference>`       | Time of the last data modification of the current file is more recent than that of the last data modification of the reference file.                                                                                                         |
| `-newer<X><Y> <reference>` | Succeeds if timestamp `X` of the file being considered is newer than timestamp `Y` of the file reference. Allowed values for `X` and `Y` are: `a` (access time), `B` (birth time, if stored), `c` (inode status change), `m` (modification). |
| `-used <num>`              | File was last accessed `num` days after its status was last changed.                                                                                                                                                                         |
| `-empty`                   | File is empty and is either a regular file or a directory.                                                                                                                                                                                   |
| `-size <num>[cwbkMG]`      | File uses `num` units of space, rounding up. The value can be in `b` (512 byte blocks), `c` (bytes), `w` (word) or `k`, `M`, `G`.                                                                                                            |
| `-readable`                | Matches files which are readable by the current user.                                                                                                                                                                                        |
| `-writable`                | Matches files which are writable by the current user.                                                                                                                                                                                        |
| `-executable`              | Matches files which are executable and directories which are searchable by the current user.                                                                                                                                                 |
| `-nouser`                  | No user corresponds to file's numeric user ID.                                                                                                                                                                                               |
| `-uid <uid>`               | File's numeric user ID is `uid`.                                                                                                                                                                                                             |
| `-user <name>`             | File is owned by user name (numeric user ID allowed).                                                                                                                                                                                        |
| `-nogroup`                 | No group corresponds to file's numeric group ID.                                                                                                                                                                                             |
| `-gid <gid>`               | File's numeric group ID is `gid`.                                                                                                                                                                                                            |
| `-group <name>`            | File belongs to group `name` (numeric group ID also allowed).                                                                                                                                                                                |
| `-perm <mode>`             | File's permission bits are exactly `mode` (octal or symbolic `g=w`).                                                                                                                                                                         |
| `-perm -<mode>`            | All of the permission bits `mode` are set for the file.                                                                                                                                                                                      |
| `-perm /<mode>`            | Any of the permission bits `mode` are set for the file.                                                                                                                                                                                      |
| `-fstype <type>`           | File is on a filesystem of `type` type. The valid filesystem types are: nfs, tmp, ext4 and more.                                                                                                                                             |
| `-name <pattern>`          | Base of file name (without path) matches shell `pattern` pattern.                                                                                                                                                                            |
| `-iname <pattern>`         | Like `-name`, but the match is case insensitive.                                                                                                                                                                                             |
| `-path <pattern>`          | File name matches shell `pattern` pattern.                                                                                                                                                                                                   |
| `-ipath <pattern>`         | Like `-path`, but the match is case insensitive.                                                                                                                                                                                             |
| `-iregex <pattern>`        | Like `-regex`, but the match is case insensitive.                                                                                                                                                                                            |
| `-lname <pattern>`         | File is a symbolic link whose contents match shell `pattern` pattern.                                                                                                                                                                        |
| `-ilname <pattern>`        | Like `-lname`, but the match is case insensitive.                                                                                                                                                                                            |
| `-regex <pattern>`         | File name matches regular expression pattern.                                                                                                                                                                                                |
| `-samefile <name>`         | File refers to the same inode as `name`.                                                                                                                                                                                                     |
| `-inum <num>`              | File has inode number `num`. It is normally easier to use the `-samefile` test instead.                                                                                                                                                      |
| `-links <num>`             | File has `num` hard links.                                                                                                                                                                                                                   |
| `-true`                    | Always true.                                                                                                                                                                                                                                 |
| `-false`                   | Always `false`.                                                                                                                                                                                                                              |
| `-type <type>`             | File is of type: `b` (block), `c` (character), `d` (directory), `p` (named pipe), `f` (file), `l` (symbolic link), `s` (socket).                                                                                                             |
| `-xtype <type>`            | The same as `-type` unless the file is a symbolic link. Then it checks the type of the file that `-type` does not check.                                                                                                                     |

For example, the patterns `fo*` and `F??` match the file names `Foo`, `FOO`, `foo`, `fOo`, etc.
The pattern `*foo*` will also match a file called `.foobar`.

### Actions

Actions have side effects and return either true or false, usually based on whether or not they are successful.

| Expression                    | Action                                                                                                                                                                                                                                                                                                                                                                                                    |
| ----------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-print`                      | Print the found paths (default action, if no other is given).                                                                                                                                                                                                                                                                                                                                             |
| `-delete`                     | Delete the found elements. This implies the `-depth` option.                                                                                                                                                                                                                                                                                                                                              |
| `-exec <command> [<args>...]` | Execute command and return true if 0 status is returned. All following arguments to find are taken to be arguments to the command until an argument consisting of `;` is encountered. The string `{}` is replaced by the current file name being processed everywhere it occurs. Both of these constructions might need to be escaped (with a `\`) or quoted to protect them from expansion by the shell. |
| `-exec <command> {} +`        | This variant of the `-exec` action runs the specified command on the selected files, but the command line is built by appending each selected file name at the end.                                                                                                                                                                                                                                       |
| `-execdir <command>`          | Like `-exec`, but the specified command is run from the subdirectory containing the matched file, which is not normally the directory in which you started find.                                                                                                                                                                                                                                          |
| `-execdir <command> {} +`     | Like `-exec`, but the specified command is run from the subdirectory containing the matched file, which is done by calling all files in the same directory in one call.                                                                                                                                                                                                                                   |
| `-ok <command>`               | Like `-exec` but ask the user first.                                                                                                                                                                                                                                                                                                                                                                      |
| `-okdir <command>`            | Like `-execdir` but ask the user first in the same way as for `-ok`.                                                                                                                                                                                                                                                                                                                                      |
| `-ls`                         | List current file in `ls -dils` format on standard output.                                                                                                                                                                                                                                                                                                                                                |
| `-fls <file>`                 | Like `-ls` but write to file like `-fprint`.                                                                                                                                                                                                                                                                                                                                                              |
| `-print`                      | Print the full file name on the standard output, followed by a newline.                                                                                                                                                                                                                                                                                                                                   |
| `-fprint <file>`              | Print the full file name into `file` file.                                                                                                                                                                                                                                                                                                                                                                |
| `-print0`                     | Print the full file name on the standard output, followed by a null character.                                                                                                                                                                                                                                                                                                                            |
| `-fprint0 <file>`             | Like `-print0` but write to file like `-fprint`.                                                                                                                                                                                                                                                                                                                                                          |
| `-printf <format>`            | Print format on the standard output, interpreting `\` escapes and `%` directives (see below). You have to add newlines add the end yourself.                                                                                                                                                                                                                                                              |
| `-fprintf <file> <format>`    | Like `-printf` but write to file like `-fprint`.                                                                                                                                                                                                                                                                                                                                                          |
| `-prune`                      | If the file is a directory, do not descend into it. If `-depth` is given, then `-prune` has no effect.                                                                                                                                                                                                                                                                                                    |
| `-quit`                       | Exit immediately.                                                                                                                                                                                                                                                                                                                                                                                         |

**printf Formats**

| Escape     | Value                                                                                                                        |
| ---------- | ---------------------------------------------------------------------------------------------------------------------------- |
| `\a`       | Alarm bell.                                                                                                                  |
| `\b`       | Backspace.                                                                                                                   |
| `\c`       | Stop printing from this format immediately and flush the output.                                                             |
| `\f`       | Form feed.                                                                                                                   |
| `\n`       | Newline.                                                                                                                     |
| `\r`       | Carriage return.                                                                                                             |
| `\t`       | Horizontal tab.                                                                                                              |
| `\v`       | Vertical tab.                                                                                                                |
| `\0`       | ASCII NUL.                                                                                                                   |
| `\\`       | A literal backslash (`\`).                                                                                                   |
| `\<NNN>`   | The character whose ASCII code is `NNN` (octal).                                                                             |
| `\<char>`  | A character followed by any other character is treated as an ordinary character.                                             |
| `%%`       | A literal percent sign.                                                                                                      |
| `%a`       | File's last access time in the format returned by the C `ctime` function.                                                    |
| `%A<time>` | File's last access time in the format specified by `time`, which is either `@` or a directive for the C `strftime` function. |
| `%b`       | The amount of disk space used for this file in 512-byte blocks.                                                              |
| `%c`       | File's last status change time in the format returned by the C `ctime` function.                                             |
| `%C<time>` | File's last status change time in the format specified by `time`, which is the same as for `%A`.                             |
| `%d`       | File's depth in the directory tree; 0 means the file is a starting-point.                                                    |
| `%D`       | The device number on which the file exists, in decimal.                                                                      |
| `%f`       | File's name with any leading directories removed (only the last element).                                                    |
| `%F`       | Type of the filesystem the file is on; this value can be used for `-fstype`.                                                 |
| `%g`       | File's group name, or numeric group ID if the group has no name.                                                             |
| `%G`       | File's numeric group ID.                                                                                                     |
| `%h`       | Leading directories of file's name (all but the last element).                                                               |
| `%H`       | Starting-point under which file was found.                                                                                   |
| `%i`       | File's inode number (in decimal).                                                                                            |
| `%k`       | The amount of disk space used for this file in 1 KB blocks.                                                                  |
| `%l`       | Object of symbolic link (empty string if file is not a symbolic link).                                                       |
| `%m`       | File's permission bits (in octal).                                                                                           |
| `%M`       | File's permissions (in symbolic form, as for `ls`).                                                                          |
| `%n`       | Number of hard links to file.                                                                                                |
| `%p`       | File's name.                                                                                                                 |
| `%P`       | File's name with the name of the starting-point under which it was found removed.                                            |
| `%s`       | File's size in bytes.                                                                                                        |
| `%S`       | File's sparseness. This is calculated as (BLOCKSIZE\*st_blocks / st_size).                                                   |
| `%t`       | File's last modification time in the format returned by the C `ctime` function.                                              |
| `%T<time>` | File's last modification time in the format specified by `time`, which is the same as for `%A`.                              |
| `%u`       | File's user name, or numeric user ID if the user has no name.                                                                |
| `%U`       | File's numeric user ID.                                                                                                      |
| `%y`       | File's type (like in `ls -l`).                                                                                               |
| `%Y`       | File's type (like `%y`), plus follow symlinks: `L` (loop), `N` (nonexistent), `?` for any other error.                       |

**Time fields**

| Field | Value                                                                 |
| ----- | --------------------------------------------------------------------- |
| `@`   | seconds since `Jan. 1, 1970, 00:00 GMT`, with fractional part.        |
| `H`   | hour (00..23)                                                         |
| `I`   | hour (01..12)                                                         |
| `k`   | hour (0..23)                                                          |
| `l`   | hour (1..12)                                                          |
| `M`   | minute (00..59)                                                       |
| `p`   | locale's AM or PM                                                     |
| `r`   | time, 12-hour (`hh:mm:ss [AP]M`)                                      |
| `S`   | Second (00.00 .. 61.00).                                              |
| `T`   | time, 24-hour (`hh:mm:ss.xxxxxxxxxx`)                                 |
| `+`   | Date and time, separated by `+`, for example `2004-04-28+22:22:05.0`. |
| `X`   | locale's time representation (`H:M:S`).                               |
| `Z`   | time zone (e.g., EDT), or nothing if no time zone is determinable.    |
| `a`   | locale's abbreviated weekday name (Sun..Sat)                          |
| `A`   | locale's full weekday name, variable length (Sunday..Saturday)        |
| `b`   | locale's abbreviated month name (Jan..Dec)                            |
| `B`   | locale's full month name, variable length (January..December)         |
| `c`   | locale's date and time (`Sat Nov 04 12:02:33 EST 1989`).              |
| `d`   | day of month (01..31)                                                 |
| `D`   | date (`mm/dd/yy`)                                                     |
| `h`   | same as b                                                             |
| `j`   | day of year (001..366)                                                |
| `m`   | month (01..12)                                                        |
| `U`   | week number of year with Sunday as first day of week (00..53)         |
| `w`   | day of week (0..6)                                                    |
| `W`   | week number of year with Monday as first day of week (00..53)         |
| `x`   | locale's date representation (`mm/dd/yy`)                             |
| `y`   | last two digits of year (00..99)                                      |
| `Y`   | year (1970...)                                                        |

### Operators

Operators join together the other items within the expression using logical combinations.
Listed in order of decreasing precedence:

| Expression             | Operator                                                                                                                     |
| ---------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| `( <expr> )`           | Force precedence. Since parentheses are special to the shell, you will normally need to quote them:`\(...\)`.                |
| `! <expr>`             | True if `expr` is false. Also use shell escape here: `\!`.                                                                   |
| `-not <expr>`          | Same as `! <expr>`.                                                                                                          |
| `<expr1> <expr2>`      | Two expressions in a row are taken to be logical joined with an implied `-a`. `expr2` is only evaluated if `expr1` is false. |
| `<expr1> -a <expr2>`   | Same as `<expr1> <expr2>`.                                                                                                   |
| `<expr1> -and <expr2>` | Same as `<expr1> <expr2>`.                                                                                                   |
| `<expr1> -o <expr2>`   | Logical or: `expr2` is not evaluated if `expr1` is true.                                                                     |
| `<expr1> -or <expr2>`  | Same as `<expr1> -o <expr2>`.                                                                                                |
| `<expr1> , <expr2>`    | List: both `expr1` and `expr2` are always evaluated.                                                                         |

## Examples

To skip the directory `src/emacs` and all files and directories under it, but print the names of the other files found:

```bash
find . -path ./src/emacs -prune -o -print
```

{!docs/assets/abbreviations.txt!}
