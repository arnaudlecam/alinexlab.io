# openssl

## Installation

=== "Debian/Ubuntu"

    ```bash
    sudo apt update
    sudo apt install openssl
    ```

## Self-Signed Certificate

Such certificates are often used internally.
It's easy to create a self-signed certificate. You just use the openssl req command. It can be tricky to create one that can be consumed by the largest selection of clients, like browsers and command line tools.

The restrictions arise in different areas:

- Modern browsers want a certificate that chains back to a trust anchor.
- And they want DNS names to be presented in particular ways in the certificate.
- And browsers are actively moving against self-signed server certificates.

First to create such a certificate run:

```bash
domain=alinex.local
openssl req -newkey rsa:4096 \
  -x509 -sha256 -days 3650 -nodes \
  -out /etc/ssl/private/$domain.crt
  -keyout /etc/ssl/private/$domain.key
  -subj "/CN=$domain" \
  -addext "subjectAltName=DNS:$domain,IP:10.0.0.1"
```

The parameters are used:

- `-newkey rsa:4096` - Creates a new certificate request and 4096 bit RSA key. The default one is 2048 bits.
- `-x509` - Creates a X.509 Certificate.
- `-sha256` - Use 265-bit SHA (Secure Hash Algorithm).
- `-days 3650` - The number of days to certify the certificate for. 3650 is ten years. You can use any positive integer.
- `-nodes` - Creates a key without a passphrase.
- `-out .../$domain.crt` - Specifies the filename to write the newly created certificate to. You can specify any file name.
- `-keyout .../$domain.key` - Specifies the filename to write the newly created private key to. You can specify any file name.
- `-subj ...` - Is used to specify details without prompting, possible are (directly put together):
  - `/C=<country>`
  - `/ST=<state>`
  - `/L=<city>`
  - `/O=<organzization>`
  - `/OU=<unit>`
  - `/CN=<domainname>`
- `-addtext "subjectAltName=..."` - Additional information like subject alternate names (SAN) (comma separated):
  - `DNS:<domain>`
  - `IP:<ip>`

It's important to put DNS name in the SAN and not the CN, because both the IETF and the CA/Browser Forums specify the practice. They also specify that DNS names in the CN are deprecated (but not prohibited). If you put a DNS name in the CN, then it must be included in the SAN under the CA/B policies. So you can't avoid using the Subject Alternate Name.
If you don't put DNS names in the SAN, then the certificate will fail to validate under a browser and other user agents which follow the CA/Browser Forum guidelines.

Now you can load this certificate in Apache, Nginx or any other server.

## Own CA

1.  Create your own authority (i.e., become a CA)
2.  Install the CA certificate on the client
3.  Create a certificate signing request (CSR) for the server
4.  Sign the server's CSR with your CA key
5.  Install the server certificate on the server

Step 1 - Create your own authority just means to create a self-signed certificate with `CA: true` and proper key usage. That means the Subject and Issuer are the same entity, CA is set to true in Basic Constraints (it should also be marked as critical), key usage is keyCertSign and crlSign (if you are using CRLs), and the Subject Key Identifier (SKI) is the same as the Authority Key Identifier (AKI).

Step 2 - import your CA into the Trust Store used by the browser.

Steps 3 - 5 are roughly what you do now for a public facing server when you enlist the services of a CA like Startcom or CAcert.

## Get Certificate

You can use openssl to extract any certificate from the server to put it into your local ssl store:

```bash
echo | openssl s_client -showcerts -connect $domain:443 2>/dev/null \
| sed -n '/BEGIN CERTIFICATE/,/END CERTIFICATE/p' \
| sudo tee /etc/ssl/$domain.pem
```

This will show details from the ssh handshake, extract the certificate in line 2 and store it in your local store.

{!docs/assets/abbreviations.txt!}
