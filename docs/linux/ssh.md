# ssh

## Open using Link

At first an `xdg-open` handler is necessary:

```bash title="~/bin/ssh-handler" hl_lines="6 7"
#!/bin/bash

d=${1#ssh://} # get destination from url

# call your preferred terminal
#gnome-terminal -e "ssh $d" &
terminator -e "ssh $d" &
```

You have to define the terminal you want to open in the last line (depends on what you have installed and what you want to use).

To test this call `~/bin/ssh-hander.sh ssh://user@host` and a new terminal should open.

Now you need a desktop handler:

```ini title="~/.local/share/applications/ssh-handler.desktop"
[Desktop Entry]
Type=Application
Name=SSH Handler
Exec=/home/alex/bin/ssh-handler %u
Icon=utilities-terminal
StartupNotify=false
MimeType=x-scheme-handler/ssh
```

And at last the application mimetype you want to use has to be linked with the desktop handler:

```ini title="~/.local/share/applications/mimeapps.list" hl_lines="5"
[Default Applications]
text/html=google-chrome.desktop
x-scheme-handler/mattermost=Mattermost.desktop
x-scheme-handler/vscode=Code.desktop
x-scheme-handler/ssh=ssh-handler.desktop
```

The last line was added therefore.

Now it should work. A click on a link like [ssh://user@192.168.100.108](ssh://user@192.168.100.108) should bring your Browser to ask what to do. Select the "SSH handler" or "xdg-open" and it should open the terminal.

{!docs/assets/abbreviations.txt!}
