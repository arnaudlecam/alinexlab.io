# Mkdocs Markdown

Pages are written in markdown Format and stored as `*.md` files within the `doc` folder. The [Markdown](https://toolchain.gitbook.com/syntax/markdown.html) implementation is nearly the same as used on GitHub but with some additions.
Use [mkdocs](../env/mkdocs.md) to convert it into static site.

## Navigation title

To set an alternative title within the navigation define it at YAML at the top:

```markdown
title: Test Page

# Test

...
```

## Simple Formats

Structure the text using headings:

```markdown
# Heading 1

## Heading 2

### Heading 3
```

Blockquotes are done using `>` signs:

```markdown
> This is a block quote in which only the first line of the paragraph needs
> to be indented by `>` but it can also be done on each line.
```

> This is a block quote in which only the first line of the paragraph needs
> to be indented by `>` but it can also be done on each line.

A horizontal line may be used to separate:

```markdown
---
```

---

Use some inline text formatting:

| Format              | Example                      | Result                             |
| ------------------- | ---------------------------- | ---------------------------------- |
| Italic              | `_Italic_` or `*Italic*`     | _Italic_                           |
| Bold                | `__Bold__` or `**Bold**`     | **Bold**                           |
| Superscript         | `H^2^O`                      | H^2^O                              |
| Subscript           | `CH~3~CH~2~OH`               | CH~3~CH~2~OH                       |
| Insert              | `^^Insert^^`                 | ^^Insert^^                         |
| Delete              | `~~Delete me~~`              | ~~Delete me~~                      |
| Mark                | `==mark me==`                | ==mark me==                        |
| Emoji               | `:smile:`                    | :smile:                            |
| Code                | \`echo "Hello"\`             | `echo "Hello"`                     |
| Code + Highlighting | \`:::bash echo "Hello"\`     | `:::bash echo "Hello"`             |
| Keys                | `++ctrl+alt+h++`             | ++ctrl+alt+h++                     |
| Link                | `[Text](http://my-site.com)` | [Text](http://my-site.com)         |
| Image               | `![Logo](../logo.png)`       | ![Logo](../assets/alinex-logo.png) |

Find a list of all keys to be used within the `++` tags in the [key map](https://github.com/facelessuser/pymdown-extensions/blob/main/pymdownx/keymap_db.py). But you can add more in the [setup](../env/mkdocs.md#pluginsandextensions)

## Icons

The following icon sets are bundled with Material for MkDocs:

-   [Material Design](https://materialdesignicons.com/)
-   [FontAwesome](https://fontawesome.com/icons?d=gallery&m=free)
-   [Octicons](https://octicons.github.com/)

```markdown
-   :material-account-circle: – `material/account-circle.svg`
-   :fontawesome-regular-laugh-wink: – `fontawesome/regular/laugh-wink.svg`
-   :octicons-repo-push-16: – `octicons/repo-push-16.svg`
```

-   :material-account-circle: – `material/account-circle.svg`
-   :fontawesome-regular-laugh-wink: – `fontawesome/regular/laugh-wink.svg`
-   :octicons-repo-push-16: – `octicons/repo-push-16.svg`

## HTML

It is also possible to add HTML tags directly. But this should be done only if there is no other way around.

!!! Example

    To add SVG images with links they have to be put into `<object>` tag:

    ```html
    <!--
    To make this svg responsive add the following in svg:
    preserveAspectRatio="xMinYMin meet" viewBox="0 0 {width} {height}"
    -->
    <object data="solutions.svg" type="image/svg+xml" width="100%">
    ![solutions overview](solutions.svg){: .center}
    </object>
    ```

## Lists

Unordered list:

```markdown
-   water
-   cola
-   beer
```

-   water
-   cola
-   beer

Ordered list:

```markdown
1. select
2. take
3. buy
```

1. select
2. take
3. buy

Definition List:

```markdown
Type 1

: This is the explanation for the first paragraph.

Type 2

: This is the explanation for the second paragraph.
```

Type 1

: This is the explanation for the first paragraph.

Type 2

: This is the explanation for the second paragraph.

And finally a task list is also possible:

```markdown
-   [x] item 1
    -   [x] item A
    -   [ ] item B
            more text
        -   [x] item a
        -   [ ] item b
        -   [x] item c
    -   [x] item C
-   [ ] item 2
-   [ ] item 3
```

-   [x] item 1
    -   [x] item A
    -   [ ] item B
            more text
        -   [x] item a
        -   [ ] item b
        -   [x] item c
    -   [x] item C
-   [ ] item 2
-   [ ] item 3

## Tables

Tables can have alignment:

```markdown
| Left | Center | Right |
| :--- | :----: | ----: |
| one  |  two   | three |
| 1    |   2    |     3 |
```

| Left | Center | Right |
| :--- | :----: | ----: |
| one  |  two   | three |
| 1    |   2    |     3 |

## Block Messages

The [Admonition](https://python-markdown.github.io/extensions/admonition/) extension configured above will allow to add text blocks.

```markdown
!!! note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

!!! note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

The different types will look like:

!!! note

    Type: `note`, `seealso`

!!! abstract

    Type: `abstract`, `summary`, `tldr`

!!! info

    Type: `info`, `todo`

!!! tip

    Type: `tip`, `hint`, `important`

!!! success

    Type: `success`, `check`, `done`

!!! question

    Type: `question`, `help`, `faq`

!!! warning

    Type: `warning`, `caution`, `attention`

!!! failure

    Type: `failure`, `fail`, `missing`

!!! danger

    Type: `danger`, `error`

!!! bug

    Type: `bug`

!!! example

    Type: `example`, `snippet`

!!! quote

    Type: `quote`, `cite`

The title can also be specified in double quotes behind the type or remove the title by setting `""` for title:

```markdown
!!! note "Individual"
```

!!! note "Individual"

The [`PyMarkdown.Details`](https://facelessuser.github.io/pymdown-extensions/extensions/details/) extension gives the same but as collapsible boxes:

```markdown
??? note "Initially closed"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

???+ note "Initially opened"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

??? note "Initially closed"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

???+ note "Initially opened"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

## Tabs

Using [Tabbed](https://facelessuser.github.io/pymdown-extensions/extensions/tabbed/) groups as tabs can be defined:

````markdown
=== "Bash"

    ```bash
    #!/bin/bash

    echo "Hello world!"
    ```

=== "Explanation"

    This is only a short example of how to make tabs.
````

=== "Bash"

    ```bash
    #!/bin/bash

    echo "Hello world!"
    ```

=== "Explanation"

    This is only a short example of how to make tabs.

If you have multiple blocks with the same tabs, their active state is shared using a script in `extra.js`.

## Code Blocks

[`Highlight`](https://facelessuser.github.io/pymdown-extensions/extensions/highlight/) will help you in display code elements.
You have multiple options to specify the language of a code block:

````markdown
```sql
SELECT count(*) FROM my_table;
```
````

```sql
SELECT count(*) FROM my_table;
```

Or by indention and using a shebang:

```markdown
    #!/bin/bash
    grep $1 $2
```

    #!/bin/bash
    grep $1 $2

Or by indention and three colon start line:

```markdown
    :::bash
    grep $1 $2
```

    :::bash
    grep $1 $2

The kline umbers can be displayed:

````markdown
```sql linenums="1"
SELECT *
FROM my_table;
```
````

```sql linenums="1"
SELECT *
FROM my_table;
```

Using [SuperFences](https://facelessuser.github.io/pymdown-extensions/extensions/superfences/) specific lines can be highlighted by passing the line numbers to the `hl_lines` argument placed right after the language identifier.

````markdown
```python hl_lines="3 4"
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```
````

```python hl_lines="3 4"
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```

Add a title:

````markdown
```python title="Bubble sort"
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```
````

```python title="Bubble sort"
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```

Adding code annotations, which will be displayed bc click:

````markdown
```yaml
theme:
    features:
        - content.code.annotate # (1)
```

1.  :man_raising_hand: I'm a code annotation! I can contain `code`, **formatted
    text**, images, ... basically anything that can be expressed in Markdown.
````

```yaml
theme:
    features:
        - content.code.annotate # (1)
```

1.  :man_raising_hand: I'm a code annotation! I can contain `code`, **formatted
    text**, images, ... basically anything that can be expressed in Markdown.

## Math

With the double dollar syntax math formulas can be added using the [AsciiMath](http://asciimath.org/) also [MathML](https://en.wikipedia.org/wiki/MathML) or [LaTeX](https://en.wikibooks.org/wiki/LaTeX/Mathematics) can be used.

It can be used inline within `$...$`:

```text
Formula: $p(x|y) = \frac{p(y|x)p(x)}{p(y)}$
```

Formula: $p(x|y) = \frac{p(y|x)p(x)}{p(y)}$

Or in a block within double `$$` like in the following examples.

=== "Latex"

    ```text
    $$
    x=\frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
    $$
    ```

    $$
    x=\frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
    $$

=== "AsciiMath"

    ```text
    $$
    x=(-b +- sqrt(b^2 - 4ac))/(2a)
    $$
    ```

    $$
    x=(-b +- sqrt(b^2 - 4ac))/(2a)
    $$

Some more examples in LaTeX are:

```text
$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$
```

$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$

A lot more is possible and as you see I prefer LaTeX because this is the most accurate and powerful syntax.

!!! Attention

    This will currently not be interpreted in PDF and printed in source format.

## Progress Bar

With the [ProgressBar](https://facelessuser.github.io/pymdown-extensions/extensions/progressbar/) plugin and some CSS styling already in the extra.css you can make progress bars:

```text
[=0% "0%"]
[=5% "5%"]
[=25% "25%"]
[=45% "45%"]
[=65% "65%"]
[=85% "85%"]
[=100% "100% ready"]

[=50%]{: .thin}
```

[=0% "0%"]
[=5% "5%"]
[=25% "25%"]
[=45% "45%"]
[=65% "65%"]
[=85% "85%"]
[=100% "100% ready"]

[=50%]{: .thin}

## Diagrams

!!! warning

    Diagrams may will not be accurate in PDF export. If this is necessary for you better make fixed graphics using yEd.

Using the [blockdiag](http://blockdiag.com/en/blockdiag/examples.html) module graphics can be auto generated out of text representations. The syntax in all of them is the same, they have to be written in a block prefaced with the diagram name.
Within the block there should be no empty lines.

Because prettier will remove indention and make it less readable a short comment `<!-- prettier-ignore -->` has to be added above such code blocks.

!!! attention

    Keep in mind that if the diagrams use sub blocks with curly braces, they need to be weighted and unclosed curly braces will tend to an `ParseException: no tokens left in the stream: <EOF>` error.

### Block Diagrams

Name the blocks and use arrows between them:

```text
<!-- prettier-ignore -->
blockdiag {
   A -> B -> C -> D;
   A -> E -> F -> G;
}
```

Internal comments are also possible like shown in line 2. This will result in:

<!-- prettier-ignore -->
blockdiag {
   A -> B -> C -> D;
   A -> E -> F -> G;
}

!!! note

    If you need more precise or complexer diagrams consider using [yEd](yed.md) to draw them and import it as SVG or PNG image.

**Label Attributes**

```text
<!-- prettier-ignore -->
blockdiag {
   // Set labels to nodes.
   A [label = "foo"];
   B [label = "bar"];
   // And set text-color
   C [label = "baz"];

   // Set labels to edges. (short text only)
   A -> B [label = "click bar", textcolor="red"];
   B -> C [label = "click baz"];
   C -> A;
}
```

<!-- prettier-ignore -->
blockdiag {
   // Set labels to nodes.
   A [label = "foo"];
   B [label = "bar"];
   // And set text-color
   C [label = "baz"];

// Set labels to edges. (short text only)
A -> B [label = "click bar", textcolor="red"];
B -> C [label = "click baz"];
C -> A;
}

**Style**

You can style nodes and edges:

```text
<!-- prettier-ignore -->
blockdiag {
   // Set boder-style, backgroun-color and text-color to nodes.
   A [style = dotted];
   B [style = dashed];
   C [color = pink, style = "3,3,3,3,15,3"]; //dashed_array format style
   D [color = "#888888", textcolor="#FFFFFF"];
   // Set border-style and color to edges.
   A -> B [style = dotted];
   B -> C [style = dashed];
   C -> D [color = "red", style = "3,3,3,3,15,3"]; //dashed_array format style
   // Set numbered-badge to nodes.
   E [numbered = 99];
   // Set arrow direction to edges.
   E -> F [dir = none];
   F -> G [dir = forward];
   G -> H [dir = back];
   H -> I [dir = both];
   // Set width and height to nodes.
   K [width = 192]; // default value is 128
   L [height = 64]; // default value is 40
   // Use thick line
   J -> K [thick]
   K -> L;
}
```

<!-- prettier-ignore -->
blockdiag {
   // Set boder-style, backgroun-color and text-color to nodes.
   A [style = dotted];
   B [style = dashed];
   C [color = pink, style = "3,3,3,3,15,3"]; //dashed_array format style
   D [color = "#888888", textcolor="#FFFFFF"];
   // Set border-style and color to edges.
   A -> B [style = dotted];
   B -> C [style = dashed];
   C -> D [color = "red", style = "3,3,3,3,15,3"]; //dashed_array format style
   // Set numbered-badge to nodes.
   E [numbered = 99];
   // Set arrow direction to edges.
   E -> F [dir = none];
   F -> G [dir = forward];
   G -> H [dir = back];
   H -> I [dir = both];
   // Set width and height to nodes.
   K [width = 192]; // default value is 128
   L [height = 64]; // default value is 40
   // Use thick line
   J -> K [thick]
   K -> L;
}

!!! note

    Background images are not working within mkdocs at the moment.

**Branches and Direction**

```text
<!-- prettier-ignore -->
blockdiag {
  // branching edges to multiple children
  A -> B, C;
  // branching edges from multiple parents
  D, E -> F;
}
```

<!-- prettier-ignore -->
blockdiag {
  // branching edges to multiple children
  A -> B, C;
  // branching edges from multiple parents
  D, E -> F;
}

**Folding**

```text
<!-- prettier-ignore -->
blockdiag {
  A -> B -> C -> D -> E;
  // fold edge at C to D (D will be layouted at top level; left side)
  C -> D [folded];
}
```

<!-- prettier-ignore -->
<!-- prettier-ignore -->
blockdiag {
  A -> B -> C -> D -> E;
  // fold edge at C to D (D will be layouted at top level; left side)
  C -> D [folded];
}

**Shapes**

```text
<!-- prettier-ignore -->
blockdiag {
  // standard node shapes
  box [shape = box];
  square [shape = square];
  roundedbox [shape = roundedbox];
  dots [shape = dots];
  circle [shape = circle];
  ellipse [shape = ellipse];
  diamond [shape = diamond];
  minidiamond [shape = minidiamond];
  note [shape = note];
  mail [shape = mail];
  cloud [shape = cloud];
  actor [shape = actor];
  beginpoint [shape = beginpoint];
  endpoint [shape = endpoint];
  box -> square -> roundedbox -> dots;
  circle -> ellipse -> diamond -> minidiamond;
  note -> mail -> cloud -> actor;
  beginpoint -> endpoint;
  // node shapes for flowcharts
  condition [shape = flowchart.condition];
  database [shape = flowchart.database];
  terminator [shape = flowchart.terminator];
  input [shape = flowchart.input];
  loopin [shape = flowchart.loopin];
  loopout [shape = flowchart.loopout];
  condition -> database -> terminator -> input;
  loopin -> loopout;
}
```

<!-- prettier-ignore -->
blockdiag {
  // standard node shapes
  box [shape = box];
  square [shape = square];
  roundedbox [shape = roundedbox];
  dots [shape = dots];
  circle [shape = circle];
  ellipse [shape = ellipse];
  diamond [shape = diamond];
  minidiamond [shape = minidiamond];
  note [shape = note];
  mail [shape = mail];
  cloud [shape = cloud];
  actor [shape = actor];
  beginpoint [shape = beginpoint];
  endpoint [shape = endpoint];
  box -> square -> roundedbox -> dots;
  circle -> ellipse -> diamond -> minidiamond;
  note -> mail -> cloud -> actor;
  beginpoint -> endpoint;
  // node shapes for flowcharts
  condition [shape = flowchart.condition];
  database [shape = flowchart.database];
  terminator [shape = flowchart.terminator];
  input [shape = flowchart.input];
  loopin [shape = flowchart.loopin];
  loopout [shape = flowchart.loopout];
  condition -> database -> terminator -> input;
  loopin -> loopout;
}

**Stocked Shapes**

```text
<!-- prettier-ignore -->
blockdiag {
  // Set stacked to nodes.
  stacked [stacked];
  diamond [shape = "diamond", stacked];
  database [shape = "flowchart.database", stacked];

  stacked -> diamond -> database;
}
```

<!-- prettier-ignore -->
blockdiag {
  // Set stacked to nodes.
  stacked [stacked];
  diamond [shape = "diamond", stacked];
  database [shape = "flowchart.database", stacked];

stacked -> diamond -> database;
}

**Dots Shape**

```text
<!-- prettier-ignore -->
blockdiag {
  A -> B, C, D;
  C [shape = "dots"];
  // hide edge forward to dots node
  A -> C [style = "none"];
}
```

<!-- prettier-ignore -->
blockdiag 
  A -> B, C, D;
  C [shape = "dots"];
  // hide edge forward to dots node
  A -> C [style = "none"];
}

**Groups**

```text
<!-- prettier-ignore -->
blockdiag {
   A -> B -> C -> D;
   A -> E;
   A -> H;
   // A and B belong to first group.
   group {
      A; B;
   }
   // E, F and G belong to second group.
   group second_group {
      // Set group-label
      label = "second group";
      // Set background-color to this group.
      color = "#77FF77";
      // Set textcolor to this group
      textcolor = "#FF0000";
      E -> F -> G;
      // H and I belong to third "nested" group.
      group {
          label = "third group";
          color = "#FF0000";
          // Set group shape to 'line group' (default is box)
          shape = line;
          // Set line style (effects to 'line group' only)
          style = dashed;
          H -> I;
      }
   }
   // J belongs to second group
   J [group = second_group];
}
```

<!-- prettier-ignore -->
blockdiag {
   A -> B -> C -> D;
   A -> E;
   A -> H;
   // A and B belong to first group.
   group {
      A; B;
   }
   // E, F and G belong to second group.
   group second_group {
      // Set group-label
      label = "second group";
      // Set background-color to this group.
      color = "#77FF77";
      // Set textcolor to this group
      textcolor = "#FF0000";
      E -> F -> G;
      // H and I belong to third "nested" group.
      group {
          label = "third group";
          color = "#FF0000";
          // Set group shape to 'line group' (default is box)
          shape = line;
          // Set line style (effects to 'line group' only)
          style = dashed;
          H -> I;
      }
   }
   // J belongs to second group
   J [group = second_group];
}

**Fonts**

```text
<!-- prettier-ignore -->
blockdiag {
  // Set fontsize
  default_fontsize = 20;  // default value is 11
  A -> B [label = "large"];
  B -> C [label = "small", fontsize = 11];  // change fontsize of edge-label
  A [fontsize = 32];  // change fontsize of node-label
  group {
    label = "group label";
    fontsize = 16;  // change fontsize of group-label
    C;
  }
}
```

<!-- prettier-ignore -->
blockdiag {
  // Set fontsize
  default_fontsize = 20;  // default value is 11
  A -> B [label = "large"];
  B -> C [label = "small", fontsize = 11];  // change fontsize of edge-label
  A [fontsize = 32];  // change fontsize of node-label
  group {
    label = "group label";
    fontsize = 16;  // change fontsize of group-label
    C;
  }
}

**Attributes**

```text
<!-- prettier-ignore -->
blockdiag {
  // Set node metrix
  node_width = 200;  // default value is 128
  node_height = 100;  // default value is 40
  // Set span metrix
  span_width = 240;  // default value is 64
  span_height = 120;  // default value is 40
  // set default shape
  default_shape = roundedbox;  // default value is 'box'
  // set default colors
  default_node_color = lightblue;
  default_group_color = "#7777FF";
  default_linecolor = blue;
  default_textcolor = red;
  A -> B [label = "Use long long\nedge label"];
  A -> C;
  group {
    C;
  }
}
```

<!-- prettier-ignore -->
blockdiag {
  // Set node metrix
  node_width = 200;  // default value is 128
  node_height = 100;  // default value is 40
  // Set span metrix
  span_width = 240;  // default value is 64
  span_height = 120;  // default value is 40
  // set default shape
  default_shape = roundedbox;  // default value is 'box'
  // set default colors
  default_node_color = lightblue;
  default_group_color = "#7777FF";
  default_linecolor = blue;
  default_textcolor = red;
  A -> B [label = "Use long long\nedge label"];
  A -> C;
  group {
    C;
  }
}

**Classes**

```text
<!-- prettier-ignore -->
blockdiag {
  // Define class (list of attributes)
  class emphasis [color = pink, style = dashed];
  class redline [color = red, style = dotted];
  A -> B -> C;
  // Set class to nod
  A [class = "emphasis"];
  // Set class to edge
  A -> B [class = "redline"];
}
```

<!-- prettier-ignore -->
blockdiag {
  // Define class (list of attributes)
  class emphasis [color = pink, style = dashed];
  class redline [color = red, style = dotted];
  A -> B -> C;
  // Set class to nod
  A [class = "emphasis"];
  // Set class to edge
  A -> B [class = "redline"];
}

**Portrait mode**

```text
<!-- prettier-ignore -->
blockdiag {
  orientation = portrait
  A -> B -> C;
       B -> D;
}
```

<!-- prettier-ignore -->
blockdiag {
  orientation = portrait
  A -> B -> C;
       B -> D;
}

```text
<!-- prettier-ignore -->
blockdiag {
  A -> B -> C;
  group {
    orientation = portrait
    C -> D -> E;
  }
}
```

<!-- prettier-ignore -->
blockdiag {
  A -> B -> C;
  group {
    orientation = portrait
    C -> D -> E;
  }
}

### Sequences

```text
<!-- prettier-ignore -->
seqdiag {
    browser -> webserver [label = "GET /index.html"];
    browser <-- webserver;
    browser -> webserver [label = "POST /blog/comment"];
    webserver -> database [label = "INSERT comment"];
    webserver <-- database;
    browser <-- webserver;
    // self referenced edge
    A -> A [label = "self reference edge"];
}
```

<!-- prettier-ignore -->
seqdiag {
    browser -> webserver [label = "GET /index.html"];
    browser <-- webserver;
    browser -> webserver [label = "POST /blog/comment"];
    webserver -> database [label = "INSERT comment"];
    webserver <-- database;
    browser <-- webserver;
    // self referenced edge
    A -> A [label = "self reference edge"];
}

**Styling**

```text
<!-- prettier-ignore -->
seqdiag {
    // asynchronus edge
    A ->> B [label = "asynchronus edge"];
    B -->> C [label = "asynchronus dotted edge"];
    B <<-- C [label = "return asynchronus doted edge"];
    A <<- B [label = "return asynchronus edge"];
    // diagonal edge
    A -> B [diagonal, label = "diagonal edge"];
    A <- B [diagonal, label = "return diagonal edge"];
    // color of edge
    A -> B [label = "colored label", color = red];
}
```

<!-- prettier-ignore -->
seqdiag {
    // asynchronus edge
    A ->> B [label = "asynchronus edge"];
    B -->> C [label = "asynchronus dotted edge"];
    B <<-- C [label = "return asynchronus doted edge"];
    A <<- B [label = "return asynchronus edge"];
    // diagonal edge
    A -> B [diagonal, label = "diagonal edge"];
    A <- B [diagonal, label = "return diagonal edge"];
    // color of edge
    A -> B [label = "colored label", color = red];
}

**Special Edges**

```text
<!-- prettier-ignore -->
seqdiag {
    // self referenced edge
    A -> A [label = "self reference edge"];
    // failed edge
    A -> B [label = "failed edge", failed];
    // auto return edge
    A => B [label = "call", return = "return"];
    // nested auto return edge
    A => B => C [label = "call", return = "return"];
}
```

<!-- prettier-ignore -->
seqdiag {
    // self referenced edge
    A -> A [label = "self reference edge"];
    // failed edge
    A -> B [label = "failed edge", failed];
    // auto return edge
    A => B [label = "call", return = "return"];
    // nested auto return edge
    A => B => C [label = "call", return = "return"];
}

**Nested Sequences**

```text
<!-- prettier-ignore -->
seqdiag {
  // auto return edge
  A => B [label = "call", return = "return"];
  // nested auto return edge
  A => B => C [label = "call", return = "return"];
  // nested sequence
  A => B [label = "nested call"]{
     B => C [label = "call 1"];
     B => D [label = "call 2"];
     B => E [label = "call 3"];
  }
}
```

<!-- prettier-ignore -->
seqdiag {
  // auto return edge
  A => B [label = "call", return = "return"];
  // nested auto return edge
  A => B => C [label = "call", return = "return"];
  // nested sequence
  A => B [label = "nested call"]{
     B => C [label = "call 1"];
     B => D [label = "call 2"];
     B => E [label = "call 3"];
  }
}

**Separators**

```text
<!-- prettier-ignore -->
seqdiag {
  A -> B;
  // Separator
  === Separator line ===
  A -> B;
  // Delay separator
  ... Separator line ...
  A -> B;
}
```

<!-- prettier-ignore -->
seqdiag {
  A -> B;
  // Separator
  === Separator line ===
  A -> B;
  // Delay separator
  ... Separator line ...
  A -> B;
}

**Edge Notes**

```text
<!-- prettier-ignore -->
seqdiag {
  // Use note (put note on rightside)
  browser -> webserver [note = "request\nGET /"];
  browser <- webserver;
  // Use leftnote and rightnote
  browser -> webserver [leftnote = "send request"];
  browser <- webserver [rightnote = "send response"];
}
```

<!-- prettier-ignore -->
seqdiag {
  // Use note (put note on rightside)
  browser -> webserver [note = "request\nGET /"];
  browser <- webserver;
  // Use leftnote and rightnote
  browser -> webserver [leftnote = "send request"];
  browser <- webserver [rightnote = "send response"];
}

**Attributes**

```text
<!-- prettier-ignore -->
seqdiag {
  // Set edge metrix.
  edge_length = 300;  // default value is 192
  span_height = 80;  // default value is 40
  // Set fontsize.
  default_fontsize = 16;  // default value is 11
  // Do not show activity line
  activation = none;
  // Numbering edges automaticaly
  autonumber = True;
  // Change note color
  default_note_color = lightblue;
  browser  -> webserver [label = "GET \n/index.html"];
  browser <-- webserver [note = "Apache works!"];
}
```

<!-- prettier-ignore -->
seqdiag {
  // Set edge metrix.
  edge_length = 300;  // default value is 192
  span_height = 80;  // default value is 40
  // Set fontsize.
  default_fontsize = 16;  // default value is 11
  // Do not show activity line
  activation = none;
  // Numbering edges automaticaly
  autonumber = True;
  // Change note color
  default_note_color = lightblue;
  browser  -> webserver [label = "GET \n/index.html"];
  browser <-- webserver [note = "Apache works!"];
}

**Ordering**

```text
<!-- prettier-ignore -->
seqdiag {
  // define order of elements
  // seqdiag sorts elements by order they appear
  browser; database; webserver;
  // diagram
  browser  -> webserver [label = "GET /index.html"];
  browser <-- webserver;
  browser  -> webserver [label = "POST /blog/comment"];
              webserver  -> database [label = "INSERT comment"];
              webserver <-- database;
  browser <-- webserver;
}
```

<!-- prettier-ignore -->
seqdiag {
  // define order of elements
  // seqdiag sorts elements by order they appear
  browser; database; webserver;
  // diagram
  browser  -> webserver [label = "GET /index.html"];
  browser <-- webserver;
  browser  -> webserver [label = "POST /blog/comment"];
              webserver  -> database [label = "INSERT comment"];
              webserver <-- database;
  browser <-- webserver;
}

### Activity

```text
<!-- prettier-ignore -->
actdiag {
    write -> convert -> image
    lane user {
        label = "User"
        write [label = "Writing reST"];
        image [label = "Get diagram IMAGE"];
    }
    lane actdiag {
        convert [label = "Convert reST to Image"];
    }
}
```

<!-- prettier-ignore -->
actdiag {
    write -> convert -> image
    lane user {
        label = "User"
        write [label = "Writing reST"];
        image [label = "Get diagram IMAGE"];
    }
    lane actdiag {
        convert [label = "Convert reST to Image"];
    }
}

### Network

```text
<!-- prettier-ignore -->
nwdiag {
  network dmz {
      address = "210.x.x.x/24"
      web01 [address = "210.x.x.1"];
      web02 [address = "210.x.x.2"];
  }
  network internal {
      address = "172.x.x.x/24";
      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01;
      db02;
  }
}
```

<!-- prettier-ignore -->
nwdiag {
  network dmz {
      address = "210.x.x.x/24"
      web01 [address = "210.x.x.1"];
      web02 [address = "210.x.x.2"];
  }
  network internal {
      address = "172.x.x.x/24";
      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01;
      db02;
  }
}

**Multiple Addresses**

```text
<!-- prettier-ignore -->
nwdiag {
  network dmz {
      address = "210.x.x.x/24"
      // set multiple addresses (using comma)
      web01 [address = "210.x.x.1, 210.x.x.20"];
      web02 [address = "210.x.x.2"];
  }
  network internal {
      address = "172.x.x.x/24";
      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01;
      db02;
  }
}
```

<!-- prettier-ignore -->
nwdiag {
  network dmz {
      address = "210.x.x.x/24"
      // set multiple addresses (using comma)
      web01 [address = "210.x.x.1, 210.x.x.20"];
      web02 [address = "210.x.x.2"];
  }
  network internal {
      address = "172.x.x.x/24";
      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01;
      db02;
  }
}

**Grouping**

```text
<!-- prettier-ignore -->
nwdiag {
  network Sample_front {
    address = "192.168.10.0/24";
    // define group
    group web {
      web01 [address = ".1"];
      web02 [address = ".2"];
    }
  }
  network Sample_back {
    address = "192.168.20.0/24";
    web01 [address = ".1"];
    web02 [address = ".2"];
    db01 [address = ".101"];
    db02 [address = ".102"];
    // define network using defined nodes
    group db {
      db01;
      db02;
    }
  }
}
```

<!-- prettier-ignore -->
nwdiag {
  network Sample_front {
    address = "192.168.10.0/24";
    // define group
    group web {
      web01 [address = ".1"];
      web02 [address = ".2"];
    }
  }
  network Sample_back {
    address = "192.168.20.0/24";
    web01 [address = ".1"];
    web02 [address = ".2"];
    db01 [address = ".101"];
    db02 [address = ".102"];
    // define network using defined nodes
    group db {
      db01;
      db02;
    }
  }
}

**Peer Networks**

```text
<!-- prettier-ignore -->
nwdiag {
  inet [shape = cloud];
  inet -- router;
  network {
    router;
    web01;
    web02;
  }
}
```

<!-- prettier-ignore -->
nwdiag {
  inet [shape = cloud];
  inet -- router;
  network {
    router;
    web01;
    web02;
  }
}

### Rack

```teyt
<!-- prettier-ignore -->
rackdiag {
  // define height of rack
  16U;

  // define rack items
  1: UPS [2U];
  3: DB Server
  4: Web Server
  5: Web Server
  6: Web Server
  7: Load Balancer
  8: L3 Switch
}
```

<!-- prettier-ignore -->
rackdiag {
  // define height of rack
  16U;

// define rack items
1: UPS [2U];
3: DB Server
4: Web Server
5: Web Server
6: Web Server
7: Load Balancer
8: L3 Switch
}

**Attributes**

```text
<!-- prettier-ignore -->
rackdiag {
  // Change order of rack-number as ascending
  ascending;
  // define height of rack
  12U;
  // define description of rack
  description = "Tokyo/1234 East";
  // define rack units
  1: UPS [2U];   // define height of unit
  3: DB Server [5kg]  // define weight of unit
  4: Web Server [0.5A]  // define ampere of unit
  5: Web Server
  6: Web Server
  7: Load Balancer
  8: L3 Switch
}
```

<!-- prettier-ignore -->
rackdiag {
  // Change order of rack-number as ascending
  ascending;
  // define height of rack
  12U;
  // define description of rack
  description = "Tokyo/1234 East";
  // define rack units
  1: UPS [2U];   // define height of unit
  3: DB Server [5kg]  // define weight of unit
  4: Web Server [0.5A]  // define ampere of unit
  5: Web Server
  6: Web Server
  7: Load Balancer
  8: L3 Switch
}

**Units in Same Level**

```text
<!-- prettier-ignore -->
rackdiag {
  // define height of rack
  16U;
  // define rack items
  1: UPS [2U];
  3: DB Server
  4: Web Server 1  // put 2 units to rack-level 4
  4: Web Server 2
  5: Web Server 3
  5: Web Server 4
  7: Load Balancer
  8: L3 Switch
}
```

<!-- prettier-ignore -->
rackdiag {
  // define height of rack
  16U;
  // define rack items
  1: UPS [2U];
  3: DB Server
  4: Web Server 1  // put 2 units to rack-level 4
  4: Web Server 2
  5: Web Server 3
  5: Web Server 4
  7: Load Balancer
  8: L3 Switch
}

**Multiple Racks**

```text
<!-- prettier-ignore -->
rackdiag {
  // define 1st rack
  rack {
    16U;
    // define rack items
    1: UPS [2U];
    3: DB Server
    4: Web Server
    5: Web Server
    6: Web Server
    7: Load Balancer
    8: L3 Switch
  }
  // define 2nd rack
  rack {
    12U;
    // define rack items
    1: UPS [2U];
    3: DB Server
    4: Web Server
    5: Web Server
    6: Web Server
    7: Load Balancer
    8: L3 Switch
  }
}
```

<!-- prettier-ignore -->
rackdiag {
  // define 1st rack
  rack {
    16U;
    // define rack items
    1: UPS [2U];
    3: DB Server
    4: Web Server
    5: Web Server
    6: Web Server
    7: Load Balancer
    8: L3 Switch
  }
  // define 2nd rack
  rack {
    12U;
    // define rack items
    1: UPS [2U];
    3: DB Server
    4: Web Server
    5: Web Server
    6: Web Server
    7: Load Balancer
    8: L3 Switch
  }
}

**Unavailable Units**

```text
<!-- prettier-ignore -->
rackdiag {
  12U;
  1: Server
  2: Server
  3: Server
  4: Server
  5: N/A [8U];
}
```

<!-- prettier-ignore -->
rackdiag {
  12U;
  1: Server
  2: Server
  3: Server
  4: Server
  5: N/A [8U];
}

### Packet

```text
<!-- prettier-ignore -->
packetdiag {
  colwidth = 32
  node_height = 72
  0-15: Source Port
  16-31: Destination Port
  32-63: Sequence Number
  64-95: Acknowledgment Number
  96-99: Data Offset
  100-105: Reserved
  106: URG [rotate = 270]
  107: ACK [rotate = 270]
  108: PSH [rotate = 270]
  109: RST [rotate = 270]
  110: SYN [rotate = 270]
  111: FIN [rotate = 270]
  112-127: Window
  128-143: Checksum
  144-159: Urgent Pointer
  160-191: (Options and Padding)
  192-223: data [colheight = 3]
}
```

<!-- prettier-ignore -->
packetdiag {
  colwidth = 32
  node_height = 72
  0-15: Source Port
  16-31: Destination Port
  32-63: Sequence Number
  64-95: Acknowledgment Number
  96-99: Data Offset
  100-105: Reserved
  106: URG [rotate = 270]
  107: ACK [rotate = 270]
  108: PSH [rotate = 270]
  109: RST [rotate = 270]
  110: SYN [rotate = 270]
  111: FIN [rotate = 270]
  112-127: Window
  128-143: Checksum
  144-159: Urgent Pointer
  160-191: (Options and Padding)
  192-223: data [colheight = 3]
}

## Charts

!!! warning

    While charts are dynamically created using JavaScript they can not be exported into PDF. If this is necessary for you better add them as fixed graphic files.

As HTML in markdown is possible this can be done using external libraries like [Chart.js](https://www.chartjs.org/docs/latest/).

```text
<canvas id="myChart" width="400" height="400"></canvas>

<script src="https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js"></script>
<script>
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>
```

<canvas id="myChart" width="400" height="400"></canvas>

<script src="https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js"></script>
<script>
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)',
                'rgba(75, 192, 192, 0.6)',
                'rgba(153, 102, 255, 0.6)',
                'rgba(255, 159, 64, 0.6)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>

## Abbreviations

Abbreviations (from the extra package) are defined as an extra paragraph mostly at the end of the document:

<!-- prettier-ignore -->
```markdown
The HTML specification
is maintained by the W3C.

*[HTML]: Hyper Text Markup Language
*[W3C]: World Wide Web Consortium
```

This will be rendered with a tooltip on each occurrence of this words:

The HTML specification
is maintained by the W3C.

> Combined with the later described [includes](#include) it is also possible to place all abbreviations in a single file and include it at the end of each page.

## Footnotes

The Footnote syntax follows the generally accepted syntax of the Markdown community. You add the footnote using `[^1]` with the correct number and define the content of this footnote later:

```markdown
Footnotes[^1] have a label[^@#$%] and the footnote's content.

[^1]: This is a footnote content.
[^@#$%]: A footnote on the label: "@#\$%".
```

This will look like:

Footnotes[^1] have a label[^@#$%] and the footnote's content.

[^1]: This is a footnote content.
[^@#$%]: A footnote on the label: "@#\$%".

A footnote label must start with a caret `^` and may contain any inline text (including spaces) between a set of square brackets `[]`. Only the first caret has any special meaning.

A footnote content must start with the label followed by a colon and at least one space. The label used to define the content must exactly match the label used in the body (including capitalization and white space). The content would then follow the label either on the same line or on the next line. The content may contain multiple lines, paragraphs, code blocks, blockquotes and most any other markdown syntax. The additional lines must be indented one level (four spaces or one tab).

```markdown
[^1]: The first paragraph of the definition.

    Paragraph two of the definition.

    > A blockquote with
    > multiple lines.

        a code block

    A final paragraph.
```

## Include

It is possible to include other markdown or files with three possible statements.
The statement will be replaced by the contents of the given file.

1.  Include relative text

    ```text
    { % include "../test.yml" %}
    ```

    > No space before first percent sign to work!

2.  Include relative markdown

    ```text
    { % include-markdown "../README.md" %}
    ```

    > No space before first percent sign to work!

    The include extension will work recursively, so any included files within will also be included.
    This replacement is done prior to any other Markdown processing, so any Markdown syntax that you want can be used within your included files.

3.  Include absolute markdown

    ```text
    { !docs/test.yml!}
    ```

    > No space before first exclamation mark to work!

    The include extension will work recursively, so any included files within will also be included.
    This replacement is done prior to any other Markdown processing, so any Markdown syntax that you want can be used within your included files.

    !!! attention

        This method will not preserve indention and therefore is not possible in any blocks.

Also the first two solutions supports to specify which part of the file to insert:

```text
{ % include "../test.html"
   start="<!--intro-start-->"
   end="<!--intro-end-->"
%}
```

## Attributes

Using attributes it is possible to set the various HTML element's attributes for output.
An example attribute list might look like this:

    {: #someid .someclass somekey='some value' }

This shows the possible definitions:

-   A word which starts with `#` will set the id of an element.
-   A word which starts with `.` will be added to the list of classes assigned to an element.
-   A key/value pair `somekey='some value'` will assign that pair to the element.

This can be set on block level elements if defined on the last line of the block by itself.

```markdown
This is a paragraph.
{: #an_id .a_class }
```

The one exception is headers, as they are only ever allowed on one line. So there you neet to write:

```markdown
### A hash style header ### {: #hash }
```

If used on inline elements the attributes are defined immediately after the element without any separation:

```markdown
[link](http://example.com){: class="foo bar" title="Some title!" }
```

### Examples

Display Link as Button:

```markdown
[Button](#top){: .md-button}
```

[Button](#top){: .md-button}

Left/right align with text float on the right:

```markdown
Text
![Example](../assets/alinex-logo.png){: .left }
![Example](../assets/alinex-logo.png){: .right }
```

Text
![Example](../assets/alinex-logo.png){: .icon .left }
![Example](../assets/alinex-logo.png){: .icon .right }

Set image size to 25% of width:

```markdown
![Example](../assets/gray-books.png){: .icon }
```

![Example](../assets/gray-books.png){: .icon }

Add drop shadow:

```markdown
![Example](../assets/default.jpg){: .icon .border }
```

![Example](../assets/default.jpg){: .icon .border }

Add image zoom:

```markdown
![Example](../assets/default.jpg){: .icon .zoom }
![Example](../assets/default.jpg){: .icon .zoom2 }
```

![Example](../assets/default.jpg){: .icon .zoom }
![Example](../assets/default.jpg){: .icon .zoom2 }

> You may define more such classes in the [additional CSS](#configuration) file.

{!docs/assets/abbreviations.txt!}
