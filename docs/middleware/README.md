title: Overview

# Middleware Systems

This chapter collects description of systems which are used to make the applications run like databases, application engine, special storages and more.
You will find short overviews and a quick help for them.

{!docs/assets/abbreviations.txt!}
