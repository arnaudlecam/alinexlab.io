# Drawer

## Module Exports

- Default: [Drawer](#drawer_1)
- [AppContent](#appcontent)
- [Content](#content)
- [Header](#header)
- [Title](#title)
- [Subtitle](#subtitle)
- [Scrim](#scrim)
- Drawer: Type
- DrawerComponentDev: Type
- ScrimComponentDev: Type

### Drawer

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `variant`: 'dismissible' | 'modal' | undefined (default)
- `open` - boolean value for state of drawer if it is dismissible
- `fixed` - display as fixed position, over content and don't move with scroll position

Methods:

- `isOpen()` - check if drawer is opened
- `setOpen()` - set open state
- `getElement()` - return its top level DOM element.

Slots:

- Default: content of drawer which can contain [Header](#header) and [Content](#content)

### AppContent

This is used to position something beside the drawer. This is especially needed if the content should move to make space for the drawer.
It will resolve to a `<div>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: content beside drawer which can contain anything

### Header

Header within [Drawer](#drawer) with title and possible close icon.
It will resolve to a `<div>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: top title, which can contain [Title](#title), [Subtitle](#subtitle) or anything else like an close icon

```html
<Header>
    <Title>Menu</Title>
    <IconButton style="position:absolute;top:4px;right:4px;"
        on:click={() => (open = false)}
        class="material-icons"
    >close</IconButton>
</Header>
```

### Content

This should contain the content of the [Drawer](#drawer).
It will resolve to a `<div>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: drawer content which can contain anything

### Title

Title within the [Header](#header).
It will resolve to a `<h1>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: title name should contain only a short text

### Subtitle

Subtitle within the [Header](#header).
It will resolve to a `<h2>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: subtitle text should contain only a short text

### Scrim

A scrim is used to dim the area around the drawer.
It will resolve to a `<div>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component
- `fixed` (default: true) -

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default:

Events:

- `SMUIDrawerScrim:click` - only on modal dialog

## Examples

### Sidebar

```html
<script lang="ts">
  import Drawer, { AppContent, Content, Header, Title } from "@smui/drawer";

  let open: boolean = false;
</script>

<div class="drawer-container">
  <Drawer variant="modal" fixed bind:open>
    <header>
      <title>Menu</title>
    </header>
    <content> ... </content>
  </Drawer>

  <AppContent class="app-content">
    <main class="main-content">
      <slot />
    </main>
  </AppContent>
</div>

<style>
  * :global(.app-content) {
    flex: auto;
    overflow: auto;
    position: relative;
    flex-grow: 1;
  }
  .main-content {
    padding: 16px;
  }
</style>
```

## More Info

Drawer: [Demo](https://sveltematerialui.com/demo/drawer/) - [Material Design](https://material.io/components/navigation-drawer) - [Material Code](https://github.com/material-components/material-components-web/tree/v11.0.0/packages/mdc-drawer)

{!docs/assets/abbreviations.txt!}
