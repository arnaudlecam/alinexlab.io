title: Overview

# Svelte Material UI

The [SMUI](https://sveltematerialui.com/) looks very good to make complexer layouts.

Icons are used from [Material Icons](https://fonts.google.com/icons).

## Components

If a new component is installed you have to call `npm run prepare` once to add it to the css.

Default Attributes

- class - A CSS class string.

{!docs/assets/abbreviations.txt!}
