# Button

Buttons allow users to take actions, and make choices, with a single tap.
The Icon Button displays a button with only an image to be used mainly as quicklinks.

## Icon Button

```svelte
<script lang="ts">
    import IconButton from '@smui/icon-button';
    let lightTheme = true;
	function switchTheme() {
        lightTheme = !lightTheme
    }
</script>

<IconButton class="material-icons"
	title="Switch Theme Dark/Light"
  	on:click={switchTheme}>
  	{ lightTheme ? 'dark_mode' : 'light_mode' }
</IconButton>
```

This shows a simple IconButton with a switch and different icons depending on the current state.

```svelte
<script lang="ts">
    import IconButton, { Icon } from '@smui/icon-button';
    let pressed = false;
    let toggleClicked = 0;
</script>

<IconButton on:click={() => toggleClicked++} toggle bind:pressed>
    <Icon class="material-icons" on>alarm_on</Icon>
    <Icon class="material-icons">alarm_off</Icon>
</IconButton>
```

This second example will use two different Icons

### Slots

- default: icon name

### Properties

- `href`: `undefined` - To directly call page on click.
- `ripple`: `true` - Whether to implement a ripple for when the component is interacted with.
- `color`: `undefined` - A color passed to the ripple action.
- `toggle`: `false` - Whether the button is a toggle. A toggle button should have two Icon children, one with the on prop.
- `ariaLabelOn`: `undefined` - The ARIA label to use when the toggle is on.
- `ariaLabelOff`: `undefined` - The ARIA label to use when the toggle is off.
- `action`: `undefined` - Used in the context of a data table pagination or a dialog. This sets the button's action. ('first-page', 'prev-page', 'next-page', or 'last-page' for data table pagination, 'close' or any string for dialog)
- `pressed`: `false` - Whether the toggle button is pressed.

### Events

- `click`
- `MDCIconButtonToggle:change`

### More Info

IconButton: [Demo](https://sveltematerialui.com/demo/icon-button) - [Description](https://github.com/hperrin/svelte-material-ui/blob/master/packages/icon-button/README.md) - [Material Design](https://github.com/material-components/material-components-web/tree/v11.0.0/packages/mdc-icon-button)

Icon: [Description](https://github.com/hperrin/svelte-material-ui/blob/master/packages/common/README.md#icon) - [Material Design](https://github.com/material-components/material-components-web/tree/v11.0.0/packages/mdc-icon-button)

{!docs/assets/abbreviations.txt!}
