# Common Elements

This elements will be mostly exported within other components.

## Module Exports

- [Common Elements](#common-elements)
  - [Module Exports](#module-exports)
    - [Label](#label)
    - [Icon](#icon)
  - [More Info](#more-info)

### Label

The label contains the banner text./
It will resolve to a `<div>` tag with specific styling.

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: banner text

### Icon

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component
- `on` - Used in the context of an icon button toggle to denote the icon for when the button is on.

Methods:

- `getElement()` - return its top level DOM element.

Slots:

- Default: name of icon to use

## More Info

Common: [Demo](https://sveltematerialui.com/demo/common/) - [Material Design](https://material.io/components/navigation-drawer) - [Material Code](https://github.com/material-components/material-components-web/tree/v11.0.0/packages/mdc-drawer)

{!docs/assets/abbreviations.txt!}
