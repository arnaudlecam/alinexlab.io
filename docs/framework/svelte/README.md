title: Overview

# Svelte & Co

Svelte is a web frontend framework like VueJS, React or Angular.

Whereas other frameworks like React, Vue and Angular generally add code to your web app to make it work in the user's browser, Svelte compiles the code that you write when you build your app. In doing so, it creates very small files and fast websites. [SolidJS](https://www.solidjs.com/) is another framework using the same concept but more with React Syntax in source files and at the moment to new with a lack of support.

## Architecture

The Frontend is based on:

- [SvelteKit](https://kit.svelte.dev/) Framework with SSR
- [Svelte](https://svelte.dev/) Browser Compiling Engine
- [SMUI](https://sveltematerialui.com/) Components

{!docs/assets/abbreviations.txt!}
