# Applications

This page will collect applications which are ready to use. So install them, configure them and use them right away.

![server icon](../assets/icons/server-icon.png){: .right .icon}

## Web and REST Server

A pre-configured and opinionated but ready to use Web and REST Server which can easily be used as backend to other services or any frontend type. Together with the ALinex GUI (see below) it makes a full fledged Application..

-   [Source](https://gitlab.com/alinex/node-server)
-   [Documentation](https://alinex.gitlab.io/node-server)

![gui icon](../assets/icons/gui-icon.png){: .right .icon}

## GUI Client for Server

The Alinex GUI Client is a modern web application which can be also be used as application on nearly any platform. It is also written using TypeScript and uses a modular concept and a modern client. Build as a modular concept using Vue framework it can be build as SPA, Android or IOS App, Windows, Mac or Linux Application. It is multi lingual, has authentication and authorization already included and can be easily extended for your needs.

-   [GUI Client](https://alinex.gitlab.io/node-gui)
-   [Documentation](https://alinex.gitlab.io/node-gui)

![svelte icon](../assets/icons/svelte-icon.png){: .right .icon}

## Svelte Test

At the moment this is a Test for the Svelte Framework including SvelteKit and Material UI.

-   [GUI Client](https://alinex.gitlab.io/node-svelte)
-   [Documentation](https://alinex.gitlab.io/node-svelte)

![checkup icon](../assets/icons/checkup-icon.png){: .right .icon}

## Checkup

Based on powerful and detailed tests it can be used as standalone application or used to enhance monitoring system possibilities. It includes different tests like console/SSH linux, REST service, Simple Website, Data Structure, logfile checks or browser simulations. This have not to replace the monitoring system but assist it by using these tests within it. Another use case is to use it for manual check and repair.

-   [Source](https://gitlab.com/alinex/node-checkup)
-   [Documentation](https://alinex.gitlab.io/node-checkup)

![node-js icon](nodejs.png){: .right .icon}

## Small Tools based on NodeJS (older)

-   [dbreport](http://alinex.github.io/node-dbreport/) tool to query database and send results as email attachments
-   [mailman](http://alinex.github.io/node-mailman/) management console based on email
-   [worktime](http://alinex.github.io/node-worktime/) command line application for logging work times

{!docs/assets/abbreviations.txt!}
