# Deployment

The last part which can bring everything out of order is the deployment of new software, data changes or architectural redesign.

![Deploy Architecture](deploy.svg)

Therefore this part is displayed here too with Repository like GitLab, Subversion and Build Tools like GitLab or Jenkins to work on. The critical point here is to monitor what and then something changes.

{!docs/assets/abbreviations.txt!}
