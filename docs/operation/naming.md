title: Naming

# Naming Convention

For complex and big structures a good naming convention is an essential element. It helps to document and name elements for all to understand.

1. As often you will access an element it is a good solution to use domain name URIs for that. A human memorable name helps you to administrate it.
2. Further it allows you to change the IP address of the host or switch the service to another host without changing your configuration. In case of services this is only possible while the port and path won't change with the switch.
3. Within your documentation the name should be used to identify the page describing it. and make a fixed documentation url containing the name. This helps to crosslink and manually call it.
4. Use the name (or short name) within all tools working with it like monitoring, virtual machine management, tickets, chat... Everybody knows the element if the name is wisely selected.

You need names for:

-   hosts
-   container
-   shares / volumes
-   services
-   processes
-   databases

But it is not easy to find a correct naming schema. There are a lot of possible problems:

-   some domains should be internal
-   free TLDs can anytime be registered
-   self-signed certificates are not optimal
-   real certificates on unregistered TLDs are not possible
-   short names are needed

## Alternatives

The following table is a short summary, see the descriptions below for the full explanation.

| Feature        | Free TLD         | Public Domain     | Own CA |
| -------------- | ---------------- | ----------------- | ------ |
| Name Structure | +++              | ++                | +++    |
| Small Net      | ++               | +++               | +      |
| Big Net        | +++              | ++                | +++    |
| Complexity     | -                | --                | --     |
| Management     | +                | -                 | --     |
| TLS/SSL        | -- (self-signed) | ++ (lets encrypt) | ++     |
| Internal       | +++              | ++                | ++     |
| External       | ---              | +                 | -      |

### Free TLDs

You can create your own complete internal domain structure if you use a top level domain which is unused. Below this you make groups by using DNS zones and names. That allows you to structure really big projects and server farms.

Our example definition with 5 level deep names is:

    <name>[-<node>].<type>[.<net>][.<group>][.<sub>].<tld>

| Part    | Description        | Values (Examples)                                  |
| ------- | ------------------ | -------------------------------------------------- |
| `tld`   | free TLD           | i.e. `grp`, `intern`                               |
| `sub`   | sub organization   | can be country `us`, `de`, `fr`                    |
| `group` | IT structure group | `front`, `back`, `office`                          |
|         | projects           | `exp`, `imp`, `ana` = analysis                     |
| `net`   | network segment    | `prod`, `stage`, `test`                            |
| `type`  | type of element    | `host`, `service`, `process`, `volume`, `database` |
| `node`  | number in cluser   | `1`, `2`, ...                                      |
| `name`  | short name         | individual                                         |

If you have different versions of the same system in parallel add a version number directly to the name like `postgres13`. And don't separate it with a dash because there the cluster number will be.

-   hosts - used within the `CNAME` entries and to access the host
    -   `postgres.host.prod.us.grp` - single host to access or cluster name with multiple DNS entries
    -   `postgres-1.host.prod.eu.grp` - first node in a cluster
    -   `storage.host.eu.grp` - production host
    -   `storage.host.staging.eu.grp` - staging host (pre production)
    -   `storage.host.test.eu.grp` - test host
-   container - the name helps to identify it for administrators
    -   `db.container.prod.eu.grp`
-   volumes - used to access the
    -   `media.volume.prod.eu.grp`
-   services
    -   `backoffice.service.prod.eu.grp`
-   processes
    -   `cleanup.process.prod.eu.grp`
-   databases
    -   `media.db.prod.eu.grp`

You can make your definition also more or less level deep. And you may also use multiple TLD entries maybe for your different companies.

!!! warning

    The TLD you choose may be coming official any time.
    You won't be able to issue fully trusted certificates for that, only self-signed ones.
    Also better not use `.local` because this is special and used by apple and others.

| Problem          | Optimization                                                            |
| ---------------- | ----------------------------------------------------------------------- |
| unsecure HTTPS   | use [public domain](#publicdomain) maybe combined with [own CA](#ownca) |
| long names       | additionally use [short names](#shortnames)                             |
| group by sorting | like in java code or container names use [reverse names](#reversenames) |
| public access    | you need an alternative public DNS entry for that                       |

### Public Domains

Use official names so that you can later also use official certificates and keep the structure flat if you want to use official certificates. This also allows to use some of the names for public access.

For the flat structure we put the different parts together with dashes:

    <name>[-<node>]-<type>[-<net>][-<group>][-<sub>].<domain>

| Part     | Description        | Values (Examples)                                  |
| -------- | ------------------ | -------------------------------------------------- |
| `domain` | public domain      | i.e. `alinex.de`                                   |
| `sub`    | sub organization   | can be country `us`, `de`, `fr`                    |
| `group`  | IT structure group | `front`, `back`, `office`                          |
|          | projects           | `exp`, `imp`, `ana` = analysis                     |
| `net`    | network segment    | `prod`, `stage`, `test`                            |
| `type`   | type of element    | `host`, `service`, `process`, `volume`, `database` |
| `node`   | number in cluser   | `1`, `2`, ...                                      |
| `name`   | short name         | individual                                         |

If you have different versions of the same system in parallel add a version number directly to the name like `postgres13`. And don't separate it with a dash because there the cluster number will be.

Examples:

-   hosts - used within the `CNAME` entries and to access the host
    -   `postgres-host.alinex.de` - single host to access or cluster name with multiple DNS entries
    -   `postgres-1-host.alinex.de` - first node in a cluster
    -   `storage-host.alinex.de` - production host
    -   `storage-staging-host.alinex.de` - staging host (pre production)
    -   `storage-test-host.alinex.de` - test host
-   container - the name helps to identify it for administrators
    -   `db-container.alinex.de`
-   volumes - used to access the
    -   `media-volume.alinex.de`
-   services - no type here to make it shorter for the web access
    -   `backoffice.alinex.de`
-   processes
    -   `cleanup-process.alinex.de`
-   databases
    -   `media-db.alinex.de`

This allows to use the following URLs:

-   hosts - `ssh://operator@db-host.alinex.de/etc/postgresql/11/main/pg_hba.conf`
-   volumes - `smb://operator@media-volume.alinex.de/export/icons`
-   services - `https://backoffice.alinex.de/api/`

Or you can identify and document the URIs:

-   databases - `postgresql://media-db.alinex.de/media/<schema>.<table>#<record-id>`

To protect the DNS names, use an internal and external DNS. Now you can decide between two alternatives:

1.  Use a public IP and public + internal DNS for a service
2.  or use only private IP and the internal DNS

If you have multiple setups for different purpose or clients you can use a different domain area for each of them.

| Problem    | Optimization                                                                                                     |
| ---------- | ---------------------------------------------------------------------------------------------------------------- |
| long names | use as less as possible elements                                                                                 |
| SSL/TLS    | this can be done using a wildcard certificate from any authority or [Let's Encrypt](https://letsencrypt.org/de/) |

### Own CA

This can be done as a root CA for the [free TLD](#freetld) you've selected. But that brings the same problems with it, it is self-signed and you have to add the CA certificate to each client or ignore their warnings.

Another alternative is to use it with a [public domain](#publicdomain) and a real certificate as subordinate CA. This allows you to be able to issue multiple certificates in different name depth with only one you have to order externally.

Here you have more to do with issuing new certificates and renewal.

## Reverse Names

Often you need to organize the names in a list like:

-   java class names
-   container names
-   virtual machine list
-   tests
-   and more

If you use the names from the [free TLD](#freetlds) you can easily reverse the DNS sections to get this. But for the definition used here in [public domains](#publicdomains) you need reorder the dash separated parts.

!!! note

    The transformation should be possible in both ways programatically to allow automation.

## Short Names

Additionally to one of the naming concepts we should use a short name which is better suited as naming the element sometimes.
This is often used in virtual machine management or monitoring. The trick here is to shorten commonly known parts but keep the individual part descriptive.

If we manage multiple different organizations (I use `ax` = alinex in this examples) or completely different projects we use an organization prefix. The network can be shortend to the first letter (`p` = production, `s` = stage, `t` = test). The type may also be shortened: `host`, `srv` = service, `proc` = process, `db` = database

    [<org>-][<sub>-][<group>-]<name>[-<node>]-<type>[<net>]

| Part    | Description                       | Values (Examples)                                                      |
| ------- | --------------------------------- | ---------------------------------------------------------------------- |
| `org`   | public domain or tld              | use a shortcut, here `ax` = alinex-de                                  |
| `sub`   | sub organization                  | can be country `us`, `de`, `fr`                                        |
| `group` | IT structure group                | `front`, `back`, `office`                                              |
|         | projects                          | `exp` = export, `imp` = import, `ana` = analysis                       |
| `net`   | network segment (first character) | `p` = prod, `s` = stage, `t` = test                                    |
| `type`  | type of element (first character) | `h` = host, `s` = service, `p` = process, `v` = volume, `d` = database |
| `node`  | number in cluser                  | `1`, `2`, ...                                                          |
| `name`  | short name                        | individual                                                             |

As you see here this transformation is also made programatically with some lookup tables for the value transformations.
It is also mostly in reverse form making it better suited in ordered lists.

Examples:

-   hosts within vmware
    -   `ax-back-pg13-ht`
    -   `ax-back-pg11-1-hp`
-   services in monitoring
    -   `ax-front-auth-sp`
    -   `ax-office-cleanup-pp`

As you see in this way the length of the name is about 12-32 characters mostly, depending how short you can keep the names.

## Name Server

For the above settings nameserver entries has to be made. Best practice is to use:

-   `A` record for the individual host entry
-   multiple `CNAME` entries for the cluster name pointing to the hostname
-   `CNAME` entry for the services pointing to the hostname

{!docs/assets/abbreviations.txt!}
