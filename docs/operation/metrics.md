title: Overview

# Metrics Data Analyzation

The tools needed here has to collect the metrics, store millions of them in a time series database and allow analyzation.

[Prometheus](#prometheus) is a database with query and collection possibilities and can also alert on the metrics data. Alternatives are graphite or InfluxDB with Telegraf to collect the metrics. For visualization [Grafana](#grafana) is used.

![Metrics Architecture](metrics.svg)

Prometheus will collect and store metrics data very often wo show them in realtime. Because there will be millions of metrics they should only be stored for a limited time (maybe 30days), but depending on space more is always possible. This will give you:

- detailed performance analysis
- realtime analyzation
- analyzation over time (short to medium timerange)
- alerts on critical data

{!docs/assets/abbreviations.txt!}
