title: yEd

# yEd

yEd is a general-purpose, cross-platform diagramming program.
It is released under a proprietary software license, that allows using a single copy for free.

yEd can be used to draw many different types of diagrams, including flowcharts, network diagrams, UMLs, BPMN, mind maps, organization charts, and entity-relationship diagrams. 

yEd loads and saves diagrams from/to GraphML, but also allows to export into bitmap graphics or VML. It is also used for a lot of diagrams on this site. I always export to VML.

## Installation

=== "Arch Linux"

    You can find it in the AUR as `yed` package to be installed.

=== "Other"

    Download the package for your platform from the [developer](https://www.yworks.com/products/yed) and install it.

## Usage

The handling is a bit specific and at first you may have some problems but you will find the correct way very fast.

### Navigation

- The mouse wheel can be used to zoom out and in.
- Right mouse button and move is used to move the viewport.
- ++up++, ++down++, ++left++, ++right++ will change the current node selection.
- ++alt+up++, ++alt+down++, ++alt+left++, ++alt+right++ to change the edge selection.

### Groups

With the use of groups you can go deeper in the graph. Each group element has to sides, an outer view showing only the group element and an inner view on which you also see the nodes within. But you may also switch into the group with a double click on it to only see it's content.

### Export

Because I use graphics on dark and light themes I try to not use black or white for lines and arrows to make the graphic work with transparent background on both.

If you export it to VML or PNG you may set transparent background in the picture tab.

{!docs/assets/abbreviations.txt!}
