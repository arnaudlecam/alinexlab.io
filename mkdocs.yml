site_name: Alinex IT Reference & Guide
site_description: A book to learn everything in the area of web applications.
site_author: Alexander Schilling
copyright: Copyright &copy; 2016 - 2023 <a href="https://alinex.de">Alexander Schilling</a>

nav:
      - Home:
              - README.md
              - alinex.md
              - Blog:
                      - blog/README.md
                      - blog/2023.md
                      - blog/2022.md
                      - blog/2021.md
                      - blog/2020.md
                      - blog/2019.md
                      - blog/2018.md
                      - blog/2017.md
                      - blog/2016.md
              - LICENSE.md
              - policy.md
              - about.md

      - Linux:
              - linux/README.md
              - linux/kubuntu.md
              - linux/manjaro.md
              - linux/debian.md
              - linux/systemd.md
              - linux/kde.md
              - Tools:
                      - linux/ssh.md
                      - linux/vi.md
                      - linux/screen.md
                      - linux/find.md
                      - linux/sed.md
                      - linux/openssl.md
                      - linux/bind.md
              - linux/howto.md
              - linux/disk-resize.md

      - Middleware:
              - middleware/README.md
              - middleware/docker.md
              - middleware/apache.md
              - middleware/postgres.md
              - middleware/mongo.md

      - Environment:
              - env/README.md
              - env/freeplane.md
              - env/yed.md
              - env/git.md
              - env/gitlab.md
              - env/gitlab-ci.md
              - env/github.md
              - env/trello.md
              - env/vscode.md
              - env/mkdocs.md
              - env/keepass.md
              - Alternatives:
                      - env/atom.md
                      - env/gitbook.md

      - Languages:
              - lang/README.md
              - JavaScript:
                      - js/README.md
                      - Versions:
                              - js/spec.md
                              - js/es-next.md
                              - js/es2015.md
                              - js/es2016.md
                              - js/es2017.md
                              - js/es2018.md
                              - js/es2019.md
                              - js/es2020.md
                              - js/es2021.md
                      - Transpiler:
                              - js/typescript.md
                              - js/flow.md
                              - js/coffee.md
                              - js/coffee-styleguide.md
                      - js/nodejs.md
                      - js/npm.md
                      - js/modules.md
                      - js/deno.md
              - Rust:
                      - rust/README.md
                      - rust/start.md
                      - rust/core.md
                      - rust/modules.md
                      - rust/standard.md
                      - rust/test.md
                      - rust/directory.md
                      - rust/tools.md
                      - Crates:
                              - rust/crates/README.md
                              - rust/crates/failure.md
                              - rust/crates/clap.md
                              - rust/crates/actix.md
                              - rust/crates/actix-web.md
                              - rust/crates/diesel.md
                      - Solutions:
                              - rust/solutions/README.md
                              - rust/solutions/phantom.md
                              - rust/solutions/cache.md
              - lang/bash.md
              - lang/go.md
              - lang/regexp.md
              - lang/markdown.md
              - lang/mkdocs.md
              - lang/handlebars.md
              - lang/sass.md

      - Frameworks:
              - framework/README.md
              - Svelte:
                      - framework/svelte/README.md
                      - framework/svelte/sveltekit.md
                      - framework/svelte/svelte.md
                      - framework/svelte/smui/README.md

      - Solutions:
              - solutions/README.md
              - solutions/devops.md
              - solutions/applications.md
              - solutions/modules.md
              - Coding:
                      - solutions/quality.md
                      - solutions/app.md
                      - solutions/exitcodes.md
              - solutions/migration.md
              - solutions/playground.md

      - Operation:
              - operation/README.md
              - operation/naming.md
              - operation/status.md
              - Metrics:
                      - operation/metrics.md
                      - operation/prometheus.md
                      - operation/grafana.md
              - operation/events.md
              - operation/management.md
              - operation/deploy.md
#        - Security:
#            - patching

use_directory_urls: false

theme:
      name: material
      icon:
            logo: material/book-open-variant
      favicon: assets/favicon.ico
      language: en
      palette:
            scheme: slate
            primary: grey
            accent: dark orange
      #font:
      #  text: Lato
      #  code: Roboto Mono
      font: false
      # remove to be GDPR conform
      #    font:
      #        text: Roboto
      #        code: Roboto Mono
      features:
            - navigation.instant
            - navigation.tabs
            - navigation.tabs.sticky
            - content.code.annotate

repo_name: "alinex/alinex.gitlab.io"
repo_url: "https://gitlab.com/alinex/alinex.gitlab.io"

extra:
      manifest: "manifest.webmanifest"
      social:
            - icon: material/gitlab
              link: https://gitlab.com/alinex
            - icon: material/github
              link: https://github.com/alinex
            - icon: material/home
              link: https://alinex.de

extra_css:
      - assets/extra.css

extra_javascript:
      - assets/extra.js
      - https://polyfill.io/v3/polyfill.min.js?features=es6
      - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
      - https://cdnjs.cloudflare.com/ajax/libs/tablesort/5.2.1/tablesort.min.js

plugins:
      - search
      - awesome-pages
      - include-markdown
      - git-revision-date-localized
      - minify:
              minify_html: true
              htmlmin_opts:
                    remove_comments: true
      - with-pdf:
              cover_subtitle: A book to learn modern development and operations
              cover_logo: https://gitlab.com/alinex/alinex.gitlab.io/-/raw/master/logo/Alinex.svg
              toc_level: 3
              output_path: alinex-book.pdf

markdown_extensions:
      - extra
      - toc:
              permalink: true
      - pymdownx.caret
      - pymdownx.tilde
      - pymdownx.mark
      - admonition
      - pymdownx.details
      - pymdownx.highlight
      - pymdownx.inlinehilite
      - pymdownx.snippets
      - pymdownx.superfences
      - pymdownx.tabbed:
              alternate_style: true
      - pymdownx.betterem:
              smart_enable: all
      - pymdownx.emoji:
              emoji_index: !!python/name:materialx.emoji.twemoji
              emoji_generator: !!python/name:materialx.emoji.to_svg
      - pymdownx.keys:
              key_map:
                    {
                          "circumflex": "^",
                          "dollar": "$",
                          "percent": "%",
                          "parenthesis-left": "(",
                          "parenthesis-right": ")",
                    }
      - pymdownx.smartsymbols
      - pymdownx.tasklist:
              custom_checkbox: true
      - markdown_blockdiag:
              format: svg
      - markdown_include.include
      - pymdownx.arithmatex:
              generic: true
      - pymdownx.progressbar
